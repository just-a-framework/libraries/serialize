#pragma once

#include <serialize/is_trivially_streamable.hpp>

#include <cstddef>
#include <vector>
#include <cstdint>
#include <stdexcept>

namespace jaf::serialize
{
    enum class mode_t
    {
        serialize,
        deserialize
    };

    struct stream;

    template<typename T>
    stream& serialize(stream& s, const T& t)
    {
        return archive(s, const_cast<T&>(t));
    }

    template<typename T>
    stream& deserialize(stream& s, T& t)
    {
        return archive(s, t);
    }

    struct stream
    {
        stream(mode_t m) :
            m_{ m }
        {
        }

        virtual ~stream() = default;

        template <class T>
        stream& operator&(T& t)
        {
            using RealT = std::remove_const_t<T>;

            if constexpr(is_trivially_streamable_v<RealT>)
            {
                constexpr auto n = sizeof(T)/sizeof(std::byte);
                const auto b = reinterpret_cast<std::byte*>(const_cast<RealT*>(&t));
                
                for(auto i = 0u; i < n; ++i)
                {
                    archive(*(b + i));
                }

                return *this;
            }
            else
            {
                switch(m_)
                {
                    case mode_t::serialize:
                    {
                        return serialize(*this, const_cast<const RealT&>(t));
                    }
                    case mode_t::deserialize:
                    {
                        return deserialize(*this, const_cast<RealT&>(t));
                    }
                }
            }

            throw std::invalid_argument{"unreachable code"};
        }
        
        virtual stream& archive(std::byte& b) = 0;

    private:
        const mode_t m_;
    };
}
