#pragma once

#include <stdexcept>

namespace jaf::serialize::exceptions
{
    struct stream_exception
        : std::runtime_error
    {
        stream_exception(const std::string& what);
        stream_exception(const char* what);

        virtual ~stream_exception();
    };
}
