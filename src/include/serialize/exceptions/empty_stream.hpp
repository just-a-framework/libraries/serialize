#pragma once

#include <serialize/exceptions/stream_exception.hpp>

namespace jaf::serialize::exceptions
{
    struct empty_stream :
        stream_exception
    {
        empty_stream();
    };
}
