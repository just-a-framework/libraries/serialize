#pragma once

#include <serialize/exceptions/stream_exception.hpp>

namespace jaf::serialize::exceptions
{
    struct full_stream :
        stream_exception
    {
        full_stream();
    };
}
