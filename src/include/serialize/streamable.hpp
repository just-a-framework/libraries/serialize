#pragma once

#include <serialize/stream.hpp>

namespace jaf::serialize
{
    struct streamable
    {
        virtual stream& archive(stream&) = 0;

        virtual ~streamable();
    };

    stream& archive(stream& s, streamable& t);
}
