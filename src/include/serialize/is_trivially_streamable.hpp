#pragma once

#include <type_traits>

namespace jaf::serialize
{
    template<class T>
    struct is_trivially_streamable
    {
        inline static constexpr auto value = !std::is_class_v<T>;
    };

    template<class T>
    inline constexpr auto is_trivially_streamable_v = is_trivially_streamable<T>::value;
}
