#pragma once

#include <serialize/stream.hpp>

namespace jaf::serialize
{
    struct ostream :
        stream
    {
        ostream();
        stream& archive(std::byte& b) final;
        
        virtual ostream& operator<<(const std::byte&) = 0;
    };
}
