#pragma once

#include <serialize/stream.hpp>

namespace jaf::serialize
{
    struct istream :
        stream
    {
        istream();
        stream& archive(std::byte& b) final;
        
        virtual istream& operator>>(std::byte&) = 0;
    };
}
