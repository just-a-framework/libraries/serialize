#pragma once

#include <serialize/istream.hpp>
#include <serialize/ostream.hpp>
#include <serialize/exceptions/empty_stream.hpp>
#include <serialize/exceptions/full_stream.hpp>

#include <array>

namespace jaf::serialize::streams
{
    template<std::size_t N>
    struct array :
        istream,
        ostream
    {
        array(std::array<std::byte, N>& buffer) :
            buffer_{ buffer },
            start_{ buffer_.begin() },
            end_{ buffer_.begin() }
        {
        }

        ostream& operator<<(const std::byte& b) final
        {
            if(end_ < buffer_.end())
            {
                *(end_++) = b;
                return *this;
            }
            throw exceptions::full_stream{};
        }

        istream& operator>>(std::byte& b) final
        {
            if(start_ < end_)
            {
                b = *(start_++);
                return *this;
            }
            throw exceptions::empty_stream{};
        }

    private:
        std::array<std::byte, N>& buffer_;
        typename std::array<std::byte, N>::iterator start_;
        typename std::array<std::byte, N>::iterator end_;
    };
}