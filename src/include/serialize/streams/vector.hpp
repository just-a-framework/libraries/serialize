#pragma once

#include <serialize/istream.hpp>
#include <serialize/ostream.hpp>
#include <serialize/exceptions/empty_stream.hpp>

#include <vector>

namespace jaf::serialize::streams
{
    struct vector :
        istream,
        ostream
    {
        vector(std::vector<std::byte>& buffer) :
            buffer_{ buffer }
        {
        }

        ostream& operator<<(const std::byte& b) final
        {
            buffer_.push_back(b);
            return *this;
        }

        istream& operator>>(std::byte& b) final
        {
            if(!buffer_.empty())
            {
              b = buffer_.front();
              buffer_.erase(buffer_.begin());
              return *this;
            }
            throw exceptions::empty_stream{};
        }

    private:
        std::vector<std::byte>& buffer_;
    };
}
