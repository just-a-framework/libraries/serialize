#pragma once

#include <serialize/stream.hpp>

#include <vector>

namespace jaf::serialize
{
    template<typename T>
    stream& serialize(stream& s, const std::vector<T>& t)
    {
        auto size = static_cast<uint64_t>(t.size());
        s & size;
        for(auto& e : t)
        {
            s & e;
        }
        return s;
    }

    template<typename T>
    stream& deserialize(stream& s, std::vector<T>& t)
    {
        auto size = static_cast<uint64_t>(t.size());
        s & size;
        t.resize(size);
        
        for(auto& e : t)
        {
            s & e;
        }
        return s;
    }
}
