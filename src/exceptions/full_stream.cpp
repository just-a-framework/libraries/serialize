#include <serialize/exceptions/full_stream.hpp>

namespace jaf::serialize::exceptions
{
    full_stream::full_stream()
        : stream_exception{ "Stream is full!" }
    {
    }
}
