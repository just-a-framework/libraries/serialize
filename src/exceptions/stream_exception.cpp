#include <serialize/exceptions/stream_exception.hpp>

namespace jaf::serialize::exceptions
{
    stream_exception::stream_exception(const std::string& what)
        : std::runtime_error{ what }
    {
    }

    stream_exception::stream_exception(const char* what)
        : std::runtime_error{ what }
    {
    }

    stream_exception::~stream_exception() = default;
}
