#include <serialize/exceptions/empty_stream.hpp>

namespace jaf::serialize::exceptions
{
    empty_stream::empty_stream()
        : stream_exception{ "Stream is empty!" }
    {
    }
}
