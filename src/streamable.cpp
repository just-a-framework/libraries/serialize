#include <serialize/streamable.hpp>

namespace jaf::serialize
{
    streamable::~streamable() = default;

    stream& archive(stream& s, streamable& t)
    {
        return t.archive(s);
    }
}
