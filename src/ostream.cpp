#include <serialize/ostream.hpp>

namespace jaf::serialize
{
    ostream::ostream() :
        stream{ mode_t::serialize }
    {
    }

    stream& ostream::archive(std::byte& b)
    {
        return *this << b;
    }
}
