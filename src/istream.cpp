#include <serialize/istream.hpp>

namespace jaf::serialize
{
    istream::istream() :
        stream{ mode_t::deserialize }
    {
    }

    stream& istream::archive(std::byte& b)
    {
        return *this >> b;
    }
}
