from conans import ConanFile, CMake

class JafSerializePort(ConanFile):
    name = "jaf_serialize"
    version = "0.0.1"
    description = "Object (de)serializer library"
    url = "https://gitlab.com/just-a-framework/libraries/serialize"
    settings = "os", "compiler", "build_type", "arch"
    generators = "cmake"
    exports_sources = "src*"
    requires = "jaf_core/0.0.1@just-a-framework+libraries+core/testing", \
        "jaf_logging/0.0.1@just-a-framework+libraries+logging/testing"
    build_requires = "cmake/3.19.5"

    def _configure_cmake(self):
        cmake = CMake(self)
        cmake.definitions["CONAN_BUILD"] = "ON"
        cmake.configure(source_folder="src")
        return cmake

    def build(self):
        cmake = self._configure_cmake()
        cmake.build()

    def package(self):
        cmake = self._configure_cmake()
        cmake.install()
    
    def package_info(self):
        self.cpp_info.builddirs = ["lib/jaf/cmake"]
