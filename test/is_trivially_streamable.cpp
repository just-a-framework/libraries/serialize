#include <serialize/is_trivially_streamable.hpp>

namespace jaf::serialize::test::is_trivially_streamable
{
    struct is_trivially_streamable
        : ::jaf::testing::test_suite
    {
    };

    struct foo
    {
    };

    struct bar
    {
    };
}

namespace jaf::serialize
{
    template<>
    struct is_trivially_streamable<jaf::serialize::test::is_trivially_streamable::foo>
    {
        inline static constexpr auto value = true;
    };
}

namespace jaf::serialize::test::is_trivially_streamable
{
    TEST_F(is_trivially_streamable, primitive)
    {
        static_assert(::jaf::serialize::is_trivially_streamable_v<int>);
        EXPECT_TRUE(::jaf::serialize::is_trivially_streamable_v<int>);
    }

    TEST_F(is_trivially_streamable, specialized)
    {
        static_assert(::jaf::serialize::is_trivially_streamable_v<foo>);
        EXPECT_TRUE(::jaf::serialize::is_trivially_streamable_v<foo>);
    }

    TEST_F(is_trivially_streamable, user_defined)
    {
        static_assert(!::jaf::serialize::is_trivially_streamable_v<bar>);
        EXPECT_FALSE(::jaf::serialize::is_trivially_streamable_v<bar>);
    }
}
