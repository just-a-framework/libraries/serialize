#include <serialize/istream.hpp>

namespace jaf::serialize::test::istream
{
    struct istream
        : ::jaf::testing::test_suite
    {
    };

    struct stream
        : ::jaf::serialize::istream
    {
        MOCK_METHOD(istream&, shift_operator, (std::byte&), ());

        istream& operator>>(std::byte& b) final
        {
            return shift_operator(b);
        }
    };

    TEST_F(istream, proxy)
    {
        using ::testing::ReturnRef;

        auto s = stream{};
        auto b = std::byte{1};

        EXPECT_CALL(s, shift_operator(b))
            .WillOnce(ReturnRef(s));

        s.archive(b);
    }
}
