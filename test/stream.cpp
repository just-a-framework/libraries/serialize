#include <serialize/stream.hpp>

namespace jaf::serialize::test::stream
{
    struct stream
        : ::jaf::testing::test_suite
    {
        stream();
        ~stream();
    };

    struct mock_stream
        : ::jaf::serialize::stream
    {
        mock_stream(::jaf::serialize::mode_t m)
            : ::jaf::serialize::stream{ m }
        {
        }

        MOCK_METHOD(::jaf::serialize::stream&, archive, (std::byte&), ());
    };

    struct foo
    {
    };

    struct foo2
    {
    };

    struct bar
    {
        MOCK_METHOD(void, serialize, (), ());
        MOCK_METHOD(void, deserialize, (), ());
        MOCK_METHOD(void, archive, (), ());
    };

    bar* b;

    stream::stream()
    {
        b = new bar{};
    }

    stream::~stream()
    {
        delete b;
    }
}

namespace jaf::serialize
{
    stream& serialize(stream& s, const ::jaf::serialize::test::stream::foo&)
    {
        ::jaf::serialize::test::stream::b->serialize();
        return s;
    }

    stream& deserialize(stream& s, ::jaf::serialize::test::stream::foo&)
    {
        ::jaf::serialize::test::stream::b->deserialize();
        return s;
    }

    stream& archive(stream& s, ::jaf::serialize::test::stream::foo2&)
    {
        ::jaf::serialize::test::stream::b->archive();
        return s;
    }
}

namespace jaf::serialize::test::stream
{
    TEST_F(stream, serialize_trivial)
    {
        using ::testing::_;
        using ::testing::ReturnRef;
        using ::testing::Invoke;

        auto s = mock_stream{ ::jaf::serialize::mode_t::serialize };
        EXPECT_CALL(s, archive(_))
            .Times(4)
            .WillRepeatedly(ReturnRef(s));

        const auto d = int{ 6 };
        s & d;
    }

    TEST_F(stream, deserialize_trivial)
    {
        using ::testing::_;
        using ::testing::ReturnRef;
        using ::testing::Invoke;

        auto s = mock_stream{ ::jaf::serialize::mode_t::deserialize };
        EXPECT_CALL(s, archive(_))
            .Times(4)
            .WillRepeatedly(ReturnRef(s));

        auto d = int{ 6 };
        s & d;
    }

    TEST_F(stream, serialize_class)
    {
        using ::testing::_;
        using ::testing::ReturnRef;
        using ::testing::Invoke;

        auto s = mock_stream{ ::jaf::serialize::mode_t::serialize };
        EXPECT_CALL(s, archive(_))
            .Times(0);

        const auto d = foo{};
        EXPECT_CALL(*b, serialize)
            .Times(1);
        EXPECT_CALL(*b, deserialize)
            .Times(0);
        EXPECT_CALL(*b, archive)
            .Times(0);
        
        s & d;
    }

    TEST_F(stream, deserialize_class)
    {
        using ::testing::_;
        using ::testing::ReturnRef;
        using ::testing::Invoke;

        auto s = mock_stream{ ::jaf::serialize::mode_t::deserialize };
        EXPECT_CALL(s, archive(_))
            .Times(0);

        auto d = foo{};
        EXPECT_CALL(*b, deserialize)
            .Times(1);
        EXPECT_CALL(*b, serialize)
            .Times(0);
        EXPECT_CALL(*b, archive)
            .Times(0);
        
        s & d;
    }

    TEST_F(stream, serialize_class_archive)
    {
        using ::testing::_;
        using ::testing::ReturnRef;
        using ::testing::Invoke;

        auto s = mock_stream{ ::jaf::serialize::mode_t::serialize };
        EXPECT_CALL(s, archive(_))
            .Times(0);

        const auto d = foo2{};
        EXPECT_CALL(*b, serialize)
            .Times(0);
        EXPECT_CALL(*b, deserialize)
            .Times(0);
        EXPECT_CALL(*b, archive)
            .Times(1);
        
        s & d;
    }

    TEST_F(stream, deserialize_class_archive)
    {
        using ::testing::_;
        using ::testing::ReturnRef;
        using ::testing::Invoke;

        auto s = mock_stream{ ::jaf::serialize::mode_t::deserialize };
        EXPECT_CALL(s, archive(_))
            .Times(0);

        const auto d = foo2{};
        EXPECT_CALL(*b, serialize)
            .Times(0);
        EXPECT_CALL(*b, deserialize)
            .Times(0);
        EXPECT_CALL(*b, archive)
            .Times(1);
        
        s & d;
    }
}
