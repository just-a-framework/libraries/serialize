#include <serialize/vector.hpp>

namespace jaf::serialize::test::vector
{
    struct vector_util
        : ::jaf::testing::test_suite
    {
        vector_util();
        ~vector_util();
    };

    struct stream
        : ::jaf::serialize::stream
    {
        stream(::jaf::serialize::mode_t m)
            : ::jaf::serialize::stream{ m }
        {
        }

        MOCK_METHOD(::jaf::serialize::stream&, archive, (std::byte&), ());
    };

    struct bar
    {
        MOCK_METHOD(void, serialize, (), ());
        MOCK_METHOD(void, deserialize, (), ());
    };

    bar* b;

    struct foo
    {
    };

    vector_util::vector_util()
    {
        b = new bar{};
    }

    vector_util::~vector_util()
    {
        delete b;
    }
}

namespace jaf::serialize
{
    stream& serialize(stream& s, const ::jaf::serialize::test::vector::foo&)
    {
        ::jaf::serialize::test::vector::b->serialize();
        return s;
    }

    stream& deserialize(stream& s, ::jaf::serialize::test::vector::foo&)
    {
        ::jaf::serialize::test::vector::b->deserialize();
        return s;
    }
}

namespace jaf::serialize::test::vector
{
    TEST_F(vector_util, serialize)
    {
        using ::testing::_;
        using ::testing::ReturnRef;

        auto s = stream{::jaf::serialize::mode_t::serialize};
        ON_CALL(s, archive(_))
            .WillByDefault(ReturnRef(s));

        const auto d = std::vector<foo>{5};
        EXPECT_CALL(*b, serialize)
            .Times(5);
        EXPECT_CALL(*b, deserialize)
            .Times(0);

        s & d;
    }

    TEST_F(vector_util, deserialize)
    {
        using ::testing::_;
        using ::testing::ReturnRef;

        auto s = stream{::jaf::serialize::mode_t::deserialize};
        ON_CALL(s, archive(_))
            .WillByDefault(ReturnRef(s));

        auto d = std::vector<foo>{4};
        EXPECT_CALL(*b, serialize)
            .Times(0);
        EXPECT_CALL(*b, deserialize)
            .Times(4);
        
        s & d;
    }
}
