#include <serialize/streams/array.hpp>

namespace jaf::serialize::streams::test::array
{
    struct array
        : ::jaf::testing::test_suite
    {
    };

    TEST_F(array, same_object)
    {
        auto b = std::array<std::byte, 10>{};
        auto s = ::jaf::serialize::streams::array<10>{ b };

        const auto b1 = std::byte{6};
        s << b1;
        auto b2 = std::byte{9};
        s >> b2;

        EXPECT_EQ(b1, b2);
    }

    TEST_F(array, good_buffer)
    {
        auto b = std::array<std::byte, 10>{};
        auto s = ::jaf::serialize::streams::array<10>{ b };

        const auto b1 = std::byte{6};
        const auto b2 = std::byte{9};
        s << b1;
        s << b1;
        s << b2;
        s << b1;

        EXPECT_EQ(b.at(0), b1);
        EXPECT_EQ(b.at(1), b1);
        EXPECT_EQ(b.at(2), b2);
        EXPECT_EQ(b.at(3), b1);
    }

    TEST_F(array, multiple_object)
    {
        auto b = std::array<std::byte, 10>{};
        auto s = ::jaf::serialize::streams::array<10>{ b };

        const auto b1 = std::byte{6};
        const auto b2 = std::byte{9};
        s << b1;
        s << b2;

        auto b3 = std::byte{2};
        auto b4 = std::byte{4};
        s >> b3;
        s >> b4;

        EXPECT_EQ(b1, b3);
        EXPECT_EQ(b2, b4);
    }

    TEST_F(array, reuse)
    {
        auto b = std::array<std::byte, 10>{};
        auto s = ::jaf::serialize::streams::array<10>{ b };

        const auto b1 = std::byte{6};
        s << b1;

        auto b3 = std::byte{2};
        s >> b3;

        EXPECT_EQ(b1, b3);

        const auto b2 = std::byte{9};
        s << b2;

        auto b4 = std::byte{4};
        s >> b4;

        EXPECT_EQ(b2, b4);
    }   

    TEST_F(array, empty)
    {
        auto b = std::array<std::byte, 10>{};
        auto s = ::jaf::serialize::streams::array<10>{ b };

        auto b1 = std::byte{2};
        EXPECT_THROW(s >> b1, ::jaf::serialize::exceptions::empty_stream);
    }

    TEST_F(array, gets_empty)
    {
        auto b = std::array<std::byte, 10>{};
        auto s = ::jaf::serialize::streams::array<10>{ b };

        const auto b1 = std::byte{6};
        s << b1;

        auto b2 = std::byte{3};
        s >> b2;

        auto b3 = std::byte{2};
        EXPECT_THROW(s >> b3, ::jaf::serialize::exceptions::empty_stream);
    }

    TEST_F(array, full)
    {
        auto b = std::array<std::byte, 0>{};
        auto s = ::jaf::serialize::streams::array<0>{ b };

        const auto b1 = std::byte{2};
        EXPECT_THROW(s << b1, ::jaf::serialize::exceptions::full_stream);
    }

    TEST_F(array, gets_full)
    {
        auto b = std::array<std::byte, 1>{};
        auto s = ::jaf::serialize::streams::array<1>{ b };

        const auto b1 = std::byte{2};
        s << b1;

        const auto b2 = std::byte{5};
        EXPECT_THROW(s << b2, ::jaf::serialize::exceptions::full_stream);
    }
}
