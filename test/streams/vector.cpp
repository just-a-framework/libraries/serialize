#include <serialize/streams/vector.hpp>

namespace jaf::serialize::streams::test::vector
{
    struct vector
        : ::jaf::testing::test_suite
    {
    };

    TEST_F(vector, same_object)
    {
        auto b = std::vector<std::byte>{};
        auto s = ::jaf::serialize::streams::vector{ b };

        const auto b1 = std::byte{6};
        s << b1;
        auto b2 = std::byte{9};
        s >> b2;

        EXPECT_EQ(b1, b2);
    }

    TEST_F(vector, good_buffer)
    {
        auto b = std::vector<std::byte>{};
        auto s = ::jaf::serialize::streams::vector{ b };

        const auto b1 = std::byte{6};
        const auto b2 = std::byte{9};
        s << b1;
        s << b1;
        s << b2;
        s << b1;

        EXPECT_EQ(b.at(0), b1);
        EXPECT_EQ(b.at(1), b1);
        EXPECT_EQ(b.at(2), b2);
        EXPECT_EQ(b.at(3), b1);
    }

    TEST_F(vector, multiple_object)
    {
        auto b = std::vector<std::byte>{};
        auto s = ::jaf::serialize::streams::vector{ b };

        const auto b1 = std::byte{6};
        const auto b2 = std::byte{9};
        s << b1;
        s << b2;

        auto b3 = std::byte{2};
        auto b4 = std::byte{4};
        s >> b3;
        s >> b4;

        EXPECT_EQ(b1, b3);
        EXPECT_EQ(b2, b4);
    }

    TEST_F(vector, reuse)
    {
        auto b = std::vector<std::byte>{};
        auto s = ::jaf::serialize::streams::vector{ b };

        const auto b1 = std::byte{6};
        s << b1;

        auto b3 = std::byte{2};
        s >> b3;

        EXPECT_EQ(b1, b3);

        const auto b2 = std::byte{9};
        s << b2;

        auto b4 = std::byte{4};
        s >> b4;

        EXPECT_EQ(b2, b4);
    }   

    TEST_F(vector, empty)
    {
        auto b = std::vector<std::byte>{};
        auto s = ::jaf::serialize::streams::vector{ b };

        auto b1 = std::byte{2};
        EXPECT_THROW(s >> b1, ::jaf::serialize::exceptions::empty_stream);
    }

    TEST_F(vector, gets_empty)
    {
        auto b = std::vector<std::byte>{};
        auto s = ::jaf::serialize::streams::vector{ b };

        const auto b1 = std::byte{6};
        s << b1;

        auto b2 = std::byte{3};
        s >> b2;

        auto b3 = std::byte{2};
        EXPECT_THROW(s >> b3, ::jaf::serialize::exceptions::empty_stream);
    }
}
