#include <serialize/streamable.hpp>

namespace jaf::serialize::test::streamable
{
    struct streamable
        : ::jaf::testing::test_suite
    {
    };

    struct stream
        : ::jaf::serialize::stream
    {
        stream(::jaf::serialize::mode_t m)
            : ::jaf::serialize::stream{ m }
        {
        }

        MOCK_METHOD(::jaf::serialize::stream&, archive, (std::byte&), ());
    };

    struct bar
        : ::jaf::serialize::streamable
    {
        MOCK_METHOD(::jaf::serialize::stream&, archive, (::jaf::serialize::stream&), ());
    };

    TEST_F(streamable, serialize)
    {
        using ::testing::_;
        using ::testing::ReturnRef;
        using ::testing::Invoke;

        auto s = stream{::jaf::serialize::mode_t::serialize};
        ON_CALL(s, archive(_))
            .WillByDefault(ReturnRef(s));

        const auto d = bar{};
        EXPECT_CALL(d, archive)
            .Times(1)
            .WillRepeatedly(ReturnRef(s));

        s & d;
    }

    TEST_F(streamable, deserialize)
    {
        using ::testing::_;
        using ::testing::ReturnRef;
        using ::testing::Invoke;

        auto s = stream{::jaf::serialize::mode_t::deserialize};
        ON_CALL(s, archive(_))
            .WillByDefault(ReturnRef(s));

        auto d = bar{};
        EXPECT_CALL(d, archive)
            .Times(1)
            .WillRepeatedly(ReturnRef(s));
        
        s & d;
    }
}
