#include <serialize/ostream.hpp>

namespace jaf::serialize::test::ostream
{
    struct ostream
        : ::jaf::testing::test_suite
    {
    };

    struct stream
        : ::jaf::serialize::ostream
    {
        MOCK_METHOD(ostream&, shift_operator, (const std::byte&), ());

        ostream& operator<<(const std::byte& b) final
        {
            return shift_operator(b);
        }
    };

    TEST_F(ostream, proxy)
    {
        using ::testing::ReturnRef;

        auto s = stream{};
        auto b = std::byte{1};

        EXPECT_CALL(s, shift_operator(b))
            .WillOnce(ReturnRef(s));

        s.archive(b);
    }
}
